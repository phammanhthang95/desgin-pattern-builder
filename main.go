package main

import (
	"desgin-pattern-builder/pkg"
	"fmt"
)

func main() {
	normalBuilder := pkg.GetBuilder("normal")
	iglooBuilder := pkg.GetBuilder("igloo")

	director := pkg.NewDirector(normalBuilder)
	normalHouse := director.BuildHouse()

	fmt.Printf("Normal house door type: %s\n", normalHouse.GetDoorType())
	fmt.Printf("Normal house window type: %s\n", normalHouse.GetWindowType())
	fmt.Printf("Normal house num gloo: %d\n", normalHouse.GetNumFloor())

	director.SetBuilder(iglooBuilder)
	iglooHouse := director.BuildHouse()

	fmt.Printf("Igloo house door type: %s\n", iglooHouse.GetDoorType())
	fmt.Printf("Igloo house window type: %s\n", iglooHouse.GetWindowType())
	fmt.Printf("Igloo house num gloo: %d\n", iglooHouse.GetNumFloor())


}