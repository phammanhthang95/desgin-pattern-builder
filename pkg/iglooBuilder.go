package pkg

type iglooBuilder struct {
	windowType string
	doorType string
	floor int
}

func newIglooBuilder() *iglooBuilder {
	return &iglooBuilder{}
}

func (b *iglooBuilder) setWindowType() {
	b.windowType = "Igloo window"
}

func (b *iglooBuilder) setDoorType() {
	b.doorType = "Igloo door"
}

func (b *iglooBuilder) setNumFloor() {
	b.floor = 5
}

func (b *iglooBuilder) getHouse() House {
	return House{
		doorType: b.doorType,
		windowType: b.windowType,
		floor: b.floor,
	}
}