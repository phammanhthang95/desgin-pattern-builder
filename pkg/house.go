package pkg

type House struct {
	windowType string
	doorType string
	floor int
}

func (c *House) GetWindowType() string {
	return c.windowType
}

func (c *House) GetDoorType() string {
	return c.doorType
}

func (c *House) GetNumFloor() int {
	return c.floor
}